﻿using System.Collections.Generic;
using UnityEngine;

public class MyPhysicsBody : MonoBehaviour
{
    public float Mass;
    public float Drag;
    public float Bounciness;

    public Vector2 Velocity;

    public bool applyGravity;
    public bool isTrigger;
    public bool IsStatic;

    public float MaxFallSpeed; 

    public MyCollider2D MyCollider;
    public List<MyCollider2D> CurrentlyColliding;

    private float collisionTimeCounter;
    private Vector2 gravity = new Vector2(0, -1f);
    private SpriteRenderer sRenderer;

    private void Start()
    {
        MyCollider = GetComponent<MyCollider2D>();
        sRenderer = GetComponent<SpriteRenderer>();
        CurrentlyColliding = new List<MyCollider2D>();
    }

    
    void FixedUpdate()
    {
        if (CurrentlyColliding.Count > 0)
        {
            sRenderer.color = Color.red;
        }
        else
        {
            sRenderer.color = Color.white;
        }

        ApplyGravity();
        ApplyDrag();
        Move();
    }
    
    private void ApplyDrag()
    {
        if (IsStatic)
            return;

        Velocity *= 1 - Drag * Time.fixedDeltaTime;
    }

    public void MyTranslate(Vector2 direction)
    {        
        transform.position += (Vector3)direction;
    }

    private void ApplyGravity()
    {
        if (IsStatic || !applyGravity)
            return;

        collisionTimeCounter += Time.fixedDeltaTime;

        if (collisionTimeCounter >= MaxFallSpeed)
            collisionTimeCounter = MaxFallSpeed;

        Velocity += gravity * Mass * collisionTimeCounter * Time.fixedDeltaTime;
    }

    public void MyAddForce(Vector2 direction)
    {
        if (IsStatic)
            return;

        Velocity += direction / (Mass == 0 ? Mass = 0.001f : Mass) * Time.fixedDeltaTime;
    }

    public void MyAddImpulse(Vector2 direction)
    {
        if (IsStatic)
            return;

        Velocity += direction / (Mass == 0 ? Mass = 0.001f : Mass);
    }

    public void Move()
    {
        if (IsStatic)
            return;
        
        transform.position += (Vector3)Velocity;
    }
}
