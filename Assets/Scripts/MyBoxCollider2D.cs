﻿using UnityEngine;

public class MyBoxCollider2D : MyCollider2D
{
    public float Height;
    public float Width;

    public float MinX;
    public float MaxX;
    public float MinY;
    public float MaxY;

    void Awake()
    {
        MinX = transform.position.x - Width / 2;
        MaxX = transform.position.x + Width / 2;
        MinY = transform.position.y - Height / 2;
        MaxY = transform.position.y + Height / 2;
    }

    private void Update()
    {
        MinX = transform.position.x - Width / 2;
        MaxX = transform.position.x + Width / 2;
        MinY = transform.position.y - Height / 2;
        MaxY = transform.position.y + Height / 2;
    }

    public MyBoxCollider2D()
    {

    }

    void OnDrawGizmos()
    {
        Vector3 derp = new Vector3(Width, Height, 0);
        Gizmos.color = Color.green;
        Gizmos.DrawWireCube(transform.position, derp);
    }

    protected override void Setup()
    {
        Type = ColliderTypes.Box;
    }
}
