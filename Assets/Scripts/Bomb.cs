﻿using UnityEngine;

public class Bomb : MonoBehaviour
{
    public float ExplosionStrength;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            Explode();
            Destroy(gameObject);
        }
    }

    public void Explode()
    {
        foreach (var item in PhysicsHandler.Instance.AllPhysicsBodys)
        {
            float distance = (transform.position - item.transform.position).magnitude;

            Vector2 force = (item.transform.position - transform.position) * ExplosionStrength / (distance <= 0.5f? 0.5f: distance);

            item.MyAddForce(force);
        }
    }
}
