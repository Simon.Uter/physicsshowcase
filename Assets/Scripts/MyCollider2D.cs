﻿using UnityEngine;

public abstract class MyCollider2D : MonoBehaviour
{
    public ColliderTypes Type;

    protected void Start()
    {
        Setup();
    }

    protected abstract void Setup();
}
