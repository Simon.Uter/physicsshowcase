﻿using UnityEngine;

public class MyCircleCollider2D : MyCollider2D
{
    public float Radius;

    protected override void Setup()
    {
        Type = ColliderTypes.Circle;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        Gizmos.DrawWireSphere(transform.position, Radius);
    }
}
