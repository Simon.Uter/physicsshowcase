﻿using UnityEngine;

public class MyVector2
{
    public float X;
    public float Y;

    public MyVector2()
    {

    }

    public MyVector2(float x, float y)
    {
        X = x;
        Y = y;
    }

    public float Magnitude()
    {
        double length;

        length = Mathf.Sqrt(X * X + Y * Y);

        return (float)length;
    }

    public static MyVector2 operator +(MyVector2 one, MyVector2 two)
    {
        MyVector2 result = new MyVector2();
        result.X = one.X + two.X;
        result.Y = one.Y + two.Y;

        return result;
    }

    public static MyVector2 operator -(MyVector2 one, MyVector2 two)
    {
        MyVector2 result = new MyVector2();
        result.X = one.X - two.X;
        result.Y = one.Y - two.Y;

        return result;
    }

    public static MyVector2 operator *(MyVector2 one, MyVector2 two)
    {
        MyVector2 result = new MyVector2();
        result.X = one.X * two.X;
        result.X = one.Y * two.Y;

        return result;
    }

    public static MyVector2 operator /(MyVector2 one, MyVector2 two)
    {
        MyVector2 result = new MyVector2();
        result.X = one.X / two.X;
        result.X = one.Y / two.Y;

        return result;
    }

    public static MyVector2 operator *(MyVector2 one, float two)
    {
        MyVector2 result = new MyVector2();
        result.X = one.X * two;
        result.Y = one.Y * two;

        return result;
    }

    public static MyVector2 operator /(MyVector2 one, float two)
    {
        MyVector2 result = new MyVector2();
        result.X = one.X / two;
        result.Y = one.Y / two;

        return result;
    }

    public MyVector2 Reverse(MyVector2 one)
    {
        one.X = one.X * -1;
        one.Y = one.Y * -1;

        return one;
    }

    public static MyVector2 Zero()
    {
        MyVector2 one = new MyVector2(0, 0);
        return one;
    }

    public static MyVector2 ZeroButSimpler()
    {
        return new MyVector2(0, 0);
    }

    public float DotProduct(MyVector2 one, MyVector2 two)
    {
        one.Normalize();
        two.Normalize();

        float result = one.X * two.X + one.Y * two.Y;
        return result;
    }

    public float CrossProduct(MyVector2 one, MyVector2 two)
    {
        float result;

        result = one.X * two.Y - one.Y * two.X;
        return result;
    }

    public MyVector2 Normalize(MyVector2 one)
    {
        float distance = one.Magnitude();

        one.X = one.X / distance;
        one.Y = one.Y / distance;

        return one;
    }

    public void Normalize()
    {
        float distance = Magnitude();

        X = X / distance;
        Y = Y / distance;
    }

    public MyVector2 Up()
    {
        return new MyVector2(0, 1);
    }

    public MyVector2 Down()
    {
        return new MyVector2(0, -1);
    }

    public MyVector2 Left()
    {
        return new MyVector2(-1, 0);
    }

    public MyVector2 Right()
    {
        return new MyVector2(1, 0);
    }
}
