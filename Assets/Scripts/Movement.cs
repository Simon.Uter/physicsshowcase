﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    public float MovementSpeed;
    public float JumpStrength;

    private float horizontalInput;
    private float verticalInput;

    private MyPhysicsBody body;

    private void Start()
    {
        body = GetComponent<MyPhysicsBody>();
    }

    private void Update()
    {
        ReadInput();
    }

    private void FixedUpdate()
    {
        Move();
    }

    private void ReadInput()
    {
        horizontalInput = Input.GetAxis("Horizontal");
        verticalInput = Input.GetAxis("Vertical");

        if (Input.GetButton("Jump"))
        {
            Jump();
        }
    }

    private void Move()
    {
        if (horizontalInput != 0)
        {
            body.MyAddForce(Vector2.right * horizontalInput * MovementSpeed * Time.fixedDeltaTime);
        }
        if (verticalInput != 0)
        {
            body.MyAddForce(Vector2.up * verticalInput * MovementSpeed * Time.fixedDeltaTime);
        }
    }

    private void Jump()
    {
        body.MyAddImpulse(Vector2.up * JumpStrength);
    }
}
