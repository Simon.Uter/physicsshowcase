﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PhysicsHandler : MonoBehaviour
{
    public static PhysicsHandler Instance;

    public List<MyCollider2D> AllColliders;
    public List<MyPhysicsBody> AllPhysicsBodys;

    private void Awake()
    {
        if (!Instance)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }

        AllColliders = FindObjectsOfType<MyCollider2D>().ToList();
        AllPhysicsBodys = FindObjectsOfType<MyPhysicsBody>().ToList();
    }

    void FixedUpdate()
    {
        CheckCollisions();
    }

    /// <summary>
    /// Continuously checks for collisions between objects with MyPhysicsBody attached.
    /// </summary>
    public void CheckCollisions()
    {
        for (int i = 0; i < AllColliders.Count; i++)
        {
            if (AllPhysicsBodys[i].IsStatic)
                continue;

            for (int j = 0; j < AllColliders.Count; j++)
            {
                if (i == j)
                    continue;

                if (AllPhysicsBodys[i].MyCollider.Type == ColliderTypes.Box)
                {
                    if (AllPhysicsBodys[j].MyCollider.Type == ColliderTypes.Box)
                    {
                        if (!AllPhysicsBodys[i].CurrentlyColliding.Contains(AllPhysicsBodys[j].MyCollider))
                        {
                            if (BoxBoxCollision((MyBoxCollider2D)AllPhysicsBodys[i].MyCollider, (MyBoxCollider2D)AllPhysicsBodys[j].MyCollider))
                            {                                
                                AllPhysicsBodys[i].CurrentlyColliding.Add(AllPhysicsBodys[j].MyCollider);
                                AllPhysicsBodys[j].CurrentlyColliding.Add(AllPhysicsBodys[i].MyCollider);

                                if(AllPhysicsBodys[i].isTrigger || AllPhysicsBodys[j].isTrigger)
                                {
                                    continue;
                                }

                                Vector2 forceOne = AllPhysicsBodys[i].Velocity * AllPhysicsBodys[i].Mass;
                                Vector2 forceTwo = AllPhysicsBodys[j].Velocity * AllPhysicsBodys[j].Mass;

                                AllPhysicsBodys[i].MyAddImpulse(forceTwo);
                                AllPhysicsBodys[i].MyAddImpulse(forceOne * -AllPhysicsBodys[i].Bounciness);

                                AllPhysicsBodys[j].MyAddImpulse(forceOne);
                                AllPhysicsBodys[j].MyAddImpulse(forceTwo * -AllPhysicsBodys[j].Bounciness);
                            }
                        }

                        else if (!BoxBoxCollision((MyBoxCollider2D)AllPhysicsBodys[i].MyCollider, (MyBoxCollider2D)AllPhysicsBodys[j].MyCollider))
                        {
                            AllPhysicsBodys[i].CurrentlyColliding.Remove(AllPhysicsBodys[j].MyCollider);
                            AllPhysicsBodys[j].CurrentlyColliding.Remove(AllPhysicsBodys[i].MyCollider);
                        }
                    }

                    else if (AllPhysicsBodys[j].MyCollider.Type == ColliderTypes.Circle)
                    {
                        if (!AllPhysicsBodys[i].CurrentlyColliding.Contains(AllPhysicsBodys[j].MyCollider))
                        {
                            if (CircleBoxCollision((MyCircleCollider2D)AllPhysicsBodys[j].MyCollider, (MyBoxCollider2D)AllPhysicsBodys[i].MyCollider))
                            {
                                AllPhysicsBodys[i].CurrentlyColliding.Add(AllPhysicsBodys[j].MyCollider);
                                AllPhysicsBodys[j].CurrentlyColliding.Add(AllPhysicsBodys[i].MyCollider);

                                if (AllPhysicsBodys[i].isTrigger || AllPhysicsBodys[j].isTrigger)
                                {
                                    continue;
                                }

                                Vector2 forceOne = AllPhysicsBodys[i].Velocity * AllPhysicsBodys[i].Mass;
                                Vector2 forceTwo = AllPhysicsBodys[j].Velocity * AllPhysicsBodys[j].Mass;

                                AllPhysicsBodys[i].MyAddImpulse(forceTwo);
                                AllPhysicsBodys[i].MyAddImpulse(forceOne * -AllPhysicsBodys[i].Bounciness);

                                AllPhysicsBodys[j].MyAddImpulse(forceOne);
                                AllPhysicsBodys[j].MyAddImpulse(forceTwo * -AllPhysicsBodys[j].Bounciness);
                            }
                        }

                        else if (!CircleBoxCollision((MyCircleCollider2D)AllPhysicsBodys[j].MyCollider, (MyBoxCollider2D)AllPhysicsBodys[i].MyCollider))
                        {
                            AllPhysicsBodys[i].CurrentlyColliding.Remove(AllPhysicsBodys[j].MyCollider);
                            AllPhysicsBodys[j].CurrentlyColliding.Remove(AllPhysicsBodys[i].MyCollider);
                        }
                    }
                }

                if (AllPhysicsBodys[i].MyCollider.Type == ColliderTypes.Circle)
                {
                    if (AllPhysicsBodys[j].MyCollider.Type == ColliderTypes.Circle)
                    {
                        if (!AllPhysicsBodys[i].CurrentlyColliding.Contains(AllPhysicsBodys[j].MyCollider))
                        {
                            if (CircleCircleCollision((MyCircleCollider2D)AllPhysicsBodys[j].MyCollider, (MyCircleCollider2D)AllPhysicsBodys[i].MyCollider))
                            {
                                AllPhysicsBodys[i].CurrentlyColliding.Add(AllPhysicsBodys[j].MyCollider);
                                AllPhysicsBodys[j].CurrentlyColliding.Add(AllPhysicsBodys[i].MyCollider);

                                if (AllPhysicsBodys[i].isTrigger || AllPhysicsBodys[j].isTrigger)
                                {
                                    continue;
                                }

                                Vector2 forceOne = AllPhysicsBodys[i].Velocity * AllPhysicsBodys[i].Mass;
                                Vector2 forceTwo = AllPhysicsBodys[j].Velocity * AllPhysicsBodys[j].Mass;

                                AllPhysicsBodys[i].MyAddImpulse(forceTwo);
                                AllPhysicsBodys[i].MyAddImpulse(forceOne * -AllPhysicsBodys[i].Bounciness);

                                AllPhysicsBodys[j].MyAddImpulse(forceOne);
                                AllPhysicsBodys[j].MyAddImpulse(forceTwo * -AllPhysicsBodys[j].Bounciness);
                            }
                        }

                        else if (!CircleCircleCollision((MyCircleCollider2D)AllPhysicsBodys[j].MyCollider, (MyCircleCollider2D)AllPhysicsBodys[i].MyCollider))
                        {
                            AllPhysicsBodys[i].CurrentlyColliding.Remove(AllPhysicsBodys[j].MyCollider);
                            AllPhysicsBodys[j].CurrentlyColliding.Remove(AllPhysicsBodys[i].MyCollider);
                        }
                    }

                    if (AllPhysicsBodys[j].MyCollider.Type == ColliderTypes.Box)
                    {
                        if (!AllPhysicsBodys[i].CurrentlyColliding.Contains(AllPhysicsBodys[j].MyCollider))
                        {
                            if (CircleBoxCollision((MyCircleCollider2D)AllPhysicsBodys[i].MyCollider, (MyBoxCollider2D)AllPhysicsBodys[j].MyCollider))
                            {
                                AllPhysicsBodys[i].CurrentlyColliding.Add(AllPhysicsBodys[j].MyCollider);
                                AllPhysicsBodys[j].CurrentlyColliding.Add(AllPhysicsBodys[i].MyCollider);

                                if (AllPhysicsBodys[i].isTrigger || AllPhysicsBodys[j].isTrigger)
                                {
                                    continue;
                                }

                                Vector2 forceOne = AllPhysicsBodys[i].Velocity * AllPhysicsBodys[i].Mass;
                                Vector2 forceTwo = AllPhysicsBodys[j].Velocity * AllPhysicsBodys[j].Mass;

                                AllPhysicsBodys[i].MyAddImpulse(forceTwo);
                                AllPhysicsBodys[i].MyAddImpulse(forceOne * -AllPhysicsBodys[i].Bounciness);

                                AllPhysicsBodys[j].MyAddImpulse(forceOne);
                                AllPhysicsBodys[j].MyAddImpulse(forceTwo * -AllPhysicsBodys[j].Bounciness);
                            }
                        }
                    }

                    else if (!CircleBoxCollision((MyCircleCollider2D)AllPhysicsBodys[i].MyCollider, (MyBoxCollider2D)AllPhysicsBodys[j].MyCollider))
                    {
                        AllPhysicsBodys[i].CurrentlyColliding.Remove(AllPhysicsBodys[j].MyCollider);
                        AllPhysicsBodys[j].CurrentlyColliding.Remove(AllPhysicsBodys[i].MyCollider);
                    }
                }

            }
        }
    }

    /// <summary>
    /// Checks for a circle-circle collosion by comparing two circles radii to the distance between them.
    /// </summary>
    public bool CircleCircleCollision(MyCircleCollider2D one, MyCircleCollider2D two)
    {
        Vector2 distance = one.transform.position - two.transform.position;

        float length = distance.magnitude;
        float radii = one.Radius + two.Radius;

        if (length >= radii)
        {
            return false;
        }
        else
        {
            return true;
        }
    }

    /// <summary>
    /// Checks for a box-box collosion by comparing the minima and maxima of the boxes.
    /// </summary>
    public bool BoxBoxCollision(MyBoxCollider2D one, MyBoxCollider2D two)
    {
        if ((one.MinX <= two.MaxX && one.MaxX >= two.MinX) && (one.MinY <= two.MaxY && one.MaxY >= two.MinY))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Looks for the nearest point on the box. Then checks for collision between circle and box by comparing distance between circle and point with circles radius.
    /// </summary>
    public bool CircleBoxCollision(MyCircleCollider2D circle, MyBoxCollider2D box)
    {
        Vector2 inbetween = circle.transform.position - box.transform.position;
        
        float xDifference = Mathf.Min(Mathf.Max(circle.transform.position.x, box.MinX), box.MaxX);
        float yDifference = Mathf.Min(Mathf.Max(circle.transform.position.y, box.MinY), box.MaxY);

        Vector2 nearestPoint = new Vector2(xDifference, yDifference);
        Vector2 circlePosition = new Vector2(circle.transform.position.x, circle.transform.position.y);
        Vector2 inbetweenNearestPoint = nearestPoint - circlePosition;
       
        float distance = inbetweenNearestPoint.magnitude;

        if (distance <= circle.Radius)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
